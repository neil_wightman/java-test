/**
 * Copyright (C) 2019 Intergral Information Solutions GmbH. All Rights Reserved
 */

package com.intergral.test.simple;

/**
 *
 * @version 1.0.0
 * @author nwightma
 */
public class Main
{

    public static final void main( final String args[] ) throws InterruptedException
    {
        final SimpleTest ts = new SimpleTest( "This is a test", Integer.MAX_VALUE - 1000 );
        for( ;; )
        {
            try
            {
                ts.message( ts.newId() );
            }
            catch( Exception e )
            {
                e.printStackTrace();
            }

            Thread.sleep( 1000 );
        }
    }

}
